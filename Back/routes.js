
import { Router } from "express"
import { gerarRelatorio } from  "./controllers/relatarioController.js";
import { clienteCreate, clienteDestroy, clienteEdit, clienteIndex, clienteSearch } from "./controllers/clienteController.js"
import { vendaCreate, vendaDestroy, vendaEdit, VendaIndex, gerarRelatorioVendas, vendaSearch } from "./controllers/vendaController.js"
import { produtoCreate, produtoDestroy, produtoEdit, produtoIndex, produtoSearch} from "./controllers/produtoController.js"
import { regisUser, loginUser } from "./controllers/loginController.js"
const router = Router()

router.post('/registrar', regisUser)
      .post('/login', loginUser);
      
router.get('/clientes', clienteIndex)
      .post('/cadastrar_cliente', clienteCreate)
      .delete('/deletar/:id', clienteDestroy)
      .get('/cliente_buscar/:nome', clienteSearch)
      .put('/cliente_editar/:id', clienteEdit)

router.get('/vendas', VendaIndex)
      .post('/vender', vendaCreate)
      .delete('/venda_delete/:id', vendaDestroy)
      .get('/venda_buscar/:cliente', vendaSearch)
      .put('/venda_editar/:id', vendaEdit)
      .get('/relatorio', gerarRelatorioVendas);

router.get('/produtos', produtoIndex)
      .post('/cadastrar', produtoCreate)
      .delete('/produto_delete/:id', produtoDestroy)
      .get('/produto_buscar/:nome', produtoSearch)
      .put('/produto_editar/:id', produtoEdit)
      



export default router