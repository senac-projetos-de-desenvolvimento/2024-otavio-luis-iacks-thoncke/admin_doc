import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';

export const Login = sequelize.define('login', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  nome: {
    type: DataTypes.STRING(60),
    allowNull: false
  },
 senha: {
    type: DataTypes.STRING(100),
    allowNull: false
  }
});