import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';


export const Venda = sequelize.define('venda', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  produto: {
    type: DataTypes.STRING(60),
    allowNull: false
  },
  marca: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  cliente: {
    type: DataTypes.STRING(60),
    allowNull: false
  },
  quantidade: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
});
