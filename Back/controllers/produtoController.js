import { Op } from 'sequelize';
import { Produto} from '../models/Produto.js'

// Função para listar todos os produtos
export const produtoIndex = async (req, res) => {
  try {
    // Busca todos os produtos no banco de dados
    const produtos = await Produto.findAll()
    // Retorna os itens com status 200 (OK)
    res.status(200).json(produtos)
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    res.status(400).send(error)
  }
}

// Função para criar um novo produto
export const produtoCreate = async (req, res) => {
  // Extrai os dados do corpo da requisição
  const { nome, marca, preco, quantidade } = req.body

  // Se algum dos atributos não foi informado
  if (!nome || !marca || !preco || !quantidade ) {
    // Retorna status 400 (Bad Request) com mensagem de erro
    res.status(400).json({ id: 0, msg: "Erro... Informe os dados" })
    return
  }

  try {
    // Cria um novo produto com os dados fornecidos
    const produto = await Produto.create({
      nome, marca, preco, quantidade
    });
    // Retorna o item criado com status 201 (Created)
    res.status(201).json(produto)
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    res.status(400).send(error)
  }
}

// Função para remover um  produto
export const produtoDestroy = async (req, res) => {
  // Extrai o id dos parâmetros da requisição
  const { id } = req.params

  try {
    // Remove o  produto com o id fornecido
    await Produto.destroy({ where: { id } });
    // Retorna mensagem de sucesso com status 200 (OK)
    res.status(200).json({ msg: "Ok! Removido com Sucesso" })
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    res.status(400).send(error)
  }
}

// Função para buscar produto por nome
export const produtoSearch = async (req, res) => {
  // Extrai o nome dos parâmetros da requisição
  const { nome } = req.params;
  
  try {
    // Busca todos os produtos que contém o nome fornecido
    const produtos = await Produto.findAll({
      where: {
        nome: {
          [Op.substring]: nome
        }
      }
    });

    // Retorna os itens encontrados com status 200 (OK)
    res.status(200).json(produtos);
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    res.status(400).send(error.message); // Envia a mensagem de erro em vez do objeto de erro completo
  }
}

// Função para editar um produto
export const produtoEdit = async (req, res) => {
  // Extrai o id dos parâmetros da requisição
  const { id } = req.params; 
  // Extrai os dados do corpo da requisição
  const dadosProduto = req.body; 
  try {
    // Busca o produto pelo id
    const itemProduto = await Produto.findByPk(id);
    // Se o item não for encontrado, retorna status 404 (Not Found) com mensagem de erro
    if (!itemProduto) {
      return res.status(404).json({ error: 'produto não encontrado' });
    }
    // Atualiza o item da Produto com os novos dados
    await itemProduto.update(dadosProduto);
    // Retorna o item atualizado com status 200 (OK)
    return res.status(200).json(itemProduto);
  } catch (error) {
    // Em caso de erro, retorna status 500 (Internal Server Error) e o erro
    return res.status(500).json({ error: 'Erro ao atualizar o produto' });
  }
}

