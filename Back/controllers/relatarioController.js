import { Op } from 'sequelize';
import { Venda } from '../models/Venda.js';

// Função para gerar relatório dos produtos mais vendidos
export const gerarRelatorio = async (req, res) => {
  try {
    // Consulta para obter os produtos mais vendidos
    const produtosMaisVendidos = await Venda.findAll({
      attributes: [
        'produto',
        [sequelize.fn('SUM', sequelize.col('quantidade')), 'total_vendas']
      ],
      group: ['produto'],
      order: [[sequelize.literal('total_vendas'), 'DESC']],
    });

    // Se não houver produtos vendidos
    if (produtosMaisVendidos.length === 0) {
      return res.status(200).json({ msg: 'Nenhum produto vendido até o momento.' });
    }

    // Retorna o relatório com status 200 (OK)
    return res.status(200).json(produtosMaisVendidos);
  } catch (error) {
    // Em caso de erro, retorna status 500 (Internal Server Error) e o erro
    return res.status(500).json({ error: 'Erro ao gerar relatório', details: error.message });
  }
};
