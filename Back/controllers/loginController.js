import { Login } from '../models/Login.js'; // Importa o modelo de Login
import bcrypt from 'bcrypt'; // Para criptografar a senha
import jwt from 'jsonwebtoken'; // Para gerar o token JWT
import dotenv from 'dotenv';

dotenv.config(); // Carrega as variáveis de ambiente

export const regisUser = async (req, res) => {
  const { nome, senha, role } = req.body;

  try {
    // Verifica se o nome já está cadastrado
    const existingUser = await Login.findOne({ where: { nome } });
    if (existingUser) {
      return res.status(400).json({ error: 'Usuário já registrado' });
    }

    // Criptografa a senha
    const hashedSenha = await bcrypt.hash(senha, 10);

    // Cria o novo usuário
    const newUser = await Login.create({
      nome,
      senha: hashedSenha,
      role
    });

    res.status(201).json({ message: 'Usuário registrado com sucesso', user: newUser });
  } catch (error) {
    res.status(500).json({ error: 'Erro ao registrar o usuário' });
  }
};

export const loginUser = async (req, res) => {
  const { nome, senha } = req.body;

  try {
    // Verifica se o usuário existe
    const user = await Login.findOne({ where: { nome } });
    if (!user) {
      return res.status(400).json({ error: 'Usuário não encontrado' });
    }

    // Verifica a senha
    const validSenha = await bcrypt.compare(senha, user.senha);
    if (!validSenha) {
      return res.status(401).json({ error: 'Senha incorreta' });
    }

    // Gera o token JWT
    const token = jwt.sign({ id: user.id, role: user.role }, process.env.JWT_SECRET, {
      expiresIn: '1h' // Token expira em 1 hora
    });

    // Retorna o token e uma mensagem de sucesso
    res.status(200).json({ message: 'Login realizado com sucesso', token });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Erro ao realizar login' });
  }
};