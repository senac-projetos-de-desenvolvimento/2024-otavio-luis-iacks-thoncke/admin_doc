import { Op } from 'sequelize';
import { Venda } from '../models/Venda.js'
import { Produto } from '../models/Produto.js'

// Função para listar todos os itens da venda
export const VendaIndex = async (req, res) => {
  try {
    // Busca todos os itens da venda no banco de dados
    const vendas = await Venda.findAll()
    // Retorna os itens com status 200 (OK)
    res.status(200).json(vendas)
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    res.status(400).send(error)
  }
}

// Função para criar um novo item na venda
export const vendaCreate = async (req, res) => {
  // Extrai os dados do corpo da requisição
  const { produto, marca, cliente, quantidade } = req.body;

  console.log("Dados recebidos:", req.body);

  // Se algum dos atributos não foi informado
  if (!produto || !marca || !cliente || !quantidade) {
    // Retorna status 400 (Bad Request) com mensagem de erro
    console.log("Dados incompletos");
    res.status(400).json({ id: 0, msg: "Erro... Informe os dados" });
    return;
  }

  try {
    // Verifica se o produto existe e tem a quantidade suficiente
    const produtoExistente = await Produto.findOne({ where: { nome: produto } });
    if (!produtoExistente) {
      console.log("Produto não encontrado");
      res.status(404).json({ id: 0, msg: "Produto não encontrado" });
      return;
    }

    if (produtoExistente.quantidade < quantidade) {
      console.log("Quantidade insuficiente em estoque");
      res.status(400).json({ id: 0, msg: "Quantidade insuficiente em estoque" });
      return;
    }

    // Cria um novo item na venda com os dados fornecidos
    const venda = await Venda.create({
      produto,
      marca,
      cliente,
      quantidade,
    });

    console.log("Venda criada:", venda);

    // Atualiza a quantidade do produto na tabela Produtos
    produtoExistente.quantidade -= quantidade;
    await produtoExistente.save();

    console.log("Quantidade atualizada do produto:", produtoExistente);

    // Retorna o item criado com status 201 (Created)
    res.status(201).json(venda);
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    console.log("Erro ao criar a venda:", error.message);
    res.status(400).send(error.message); // Envia a mensagem de erro em vez do objeto de erro completo
  }
}

// Função para remover um item da venda
export const vendaDestroy = async (req, res) => {
  // Extrai o id dos parâmetros da requisição
  const { id } = req.params

  try {
    // Remove o item da venda com o id fornecido
    await Venda.destroy({ where: { id } });
    // Retorna mensagem de sucesso com status 200 (OK)
    res.status(200).json({ msg: "Ok! Removido com Sucesso" })
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    res.status(400).send(error)
  }
}

// Função para buscar itens da venda por cliente
export const vendaSearch = async (req, res) => {
  // Extrai o cliente dos parâmetros da requisição
  const { cliente } = req.params;

  console.log("Cliente recebido:", cliente);

  try {
    // Busca todos os itens da venda que contém o nome fornecido
    const vendas = await Venda.findAll({
      where: {
        cliente: {
          [Op.substring]: cliente
        }
      }
    });

    console.log("Vendas encontradas:", vendas);

    // Retorna os itens encontrados com status 200 (OK)
    res.status(200).json(vendas);
  } catch (error) {
    // Em caso de erro, retorna status 400 (Bad Request) e o erro
    console.log("Erro ao buscar vendas:", error.message);
    res.status(400).send(error.message); // Envia a mensagem de erro em vez do objeto de erro completo
  }
}


// Função para editar um item da venda
export const vendaEdit = async (req, res) => {
  // Extrai o id dos parâmetros da requisição
  const { id } = req.params; 
  // Extrai os dados do corpo da requisição
  const dadosVenda = req.body; 
  try {
    // Busca o item da venda pelo id
    const itemVenda = await Venda.findByPk(id);
    // Se o item não for encontrado, retorna status 404 (Not Found) com mensagem de erro
    if (!itemVenda) {
      return res.status(404).json({ error: 'venda não encontrada' });
    }
    // Atualiza a venda com os novos dados
    await itemVenda.update(dadosVenda);
    // Retorna o item atualizado com status 200 (OK)
    return res.status(200).json(itemVenda);
  } catch (error) {
    // Em caso de erro, retorna status 500 (Internal Server Error) e o erro
    return res.status(500).json({ error: 'Erro ao atualizar a venda' });
  }
}


export const gerarRelatorioVendas = async (req, res) => {
  try {
    // Busca todas as vendas
    const vendas = await Venda.findAll();
    
    if (vendas.length === 0) {
      return res.status(404).json({ msg: "Nenhuma venda registrada" });
    }

    // Cria um mapa para contar as vendas por produto
    const contagemProdutos = {};

    vendas.forEach((venda) => {
      const produto = venda.produto;

      // Incrementa a contagem do produto ou inicializa com 1
      contagemProdutos[produto] = (contagemProdutos[produto] || 0) + venda.quantidade;
    });

    // Determina o mais vendido e o menos vendido
    let maisVendido = null;
    let menosVendido = null;

    Object.entries(contagemProdutos).forEach(([produto, quantidade]) => {
      if (!maisVendido || quantidade > maisVendido.quantidade) {
        maisVendido = { produto, quantidade };
      }
      if (!menosVendido || quantidade < menosVendido.quantidade) {
        menosVendido = { produto, quantidade };
      }
    });

    // Retorna o relatório com o status 200 (OK)
    res.status(200).json({
      maisVendido,
      menosVendido,
      contagemCompleta: contagemProdutos, // Inclui a contagem completa para detalhamento
    });
  } catch (error) {
    // Em caso de erro, retorna status 500 (Internal Server Error) e o erro
    console.log("Erro ao gerar relatório:", error.message);
    res.status(500).json({ error: "Erro ao gerar relatório" });
  }
};
