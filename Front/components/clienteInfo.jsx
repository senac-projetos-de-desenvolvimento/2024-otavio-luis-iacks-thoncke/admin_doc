import "bootstrap-icons/font/bootstrap-icons.css";
import Swal from "sweetalert2";


export default function ClienteInfo(props) {
  function confirmaExclusao(id, nome) {
    // if (confirm(`Confirma Exclusão do Filme "${titulo}"?`)) {
    //   props.exclui(id)
    // }
    Swal.fire({
      title: `Confirma Exclusão do "${nome}"?`,
      text: "Esta operação não poderá ser desfeita",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim. Excluir!",
    }).then((result) => {
      if (result.isConfirmed) {
        props.exclui(id);
        Swal.fire("Excluído!", "produto excluído com sucesso", "success");
      }
    });
  }

  return (
    <tr>
      
      <td className={props.cliente.destaque ? "fw-bold" : ""}>
        {props.cliente.nome}
      </td>
      
      <td className={props.cliente.destaque ? "fw-bold" : ""}>
        {props.cliente.endereco}
      </td>
      
      
      <td className={props.cliente.destaque ? "fw-bold" : ""}>
        {props.cliente.cep}
      </td>
      
      
        <td>
          <i
            className="bi bi-trash-fill text-danger"
            style={{ fontSize: 24, cursor: "pointer" }}
            onClick={() =>
              confirmaExclusao(props.cliente.id, props.cliente.nome)
            }
            title="Excluir"
          ></i>
          
          <i
          className="bi bi-pencil-square text-warning ms-2"
          style={{ fontSize: 24, cursor: "pointer" }}
          onClick={props.altera}
          title="Alterar"
        ></i>

        
      </td>
    </tr>
  );
}
