import "bootstrap-icons/font/bootstrap-icons.css";
import Swal from "sweetalert2";
import ButtonLink from 'next/link';

export default function ItemLista(props) {
  function confirmaExclusao(id, nome) {
    // if (confirm(`Confirma Exclusão do Filme "${titulo}"?`)) {
    //   props.exclui(id)
    // }
    Swal.fire({
      title: `Confirma Exclusão de produto "${nome}"?`,
      text: "Esta operação não poderá ser desfeita",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim. Excluir!",
    }).then((result) => {
      if (result.isConfirmed) {
        props.exclui(id);
        Swal.fire("Excluído!", "produto excluído com sucesso", "success");
      }
    });
  }

  return (
    <tr>
      
      <td className={props.produto.destaque ? "fw-bold" : ""}>
        {props.produto.nome}
      </td>
      
      <td className={props.produto.destaque ? "fw-bold" : ""}>
        {props.produto.marca}
      </td>
      
      
      <td className={props.produto.destaque ? "fw-bold" : ""}>
        {props.produto.preco}
      </td>
      <td className={props.produto.destaque ? "fw-bold" : ""}>
        {props.produto.quantidade}
      </td>
      
        <td>
          <i
            className="bi bi-trash-fill text-danger"
            style={{ fontSize: 24, cursor: "pointer" }}
            onClick={() =>
              confirmaExclusao(props.produto.id, props.produto.nome)
            }
            title="Excluir"
          ></i>
          
          <i
          className="bi bi-pencil-square text-warning ms-2"
          style={{ fontSize: 24, cursor: "pointer" }}
          onClick={props.altera}
          title="Alterar"
        ></i>

        <ButtonLink href="/CadVenda" style={{ marginLeft: '50px' }}>Vender</ButtonLink>
      </td>
    </tr>
  );
}
