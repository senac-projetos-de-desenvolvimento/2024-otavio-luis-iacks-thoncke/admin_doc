import "bootstrap-icons/font/bootstrap-icons.css";
import Swal from "sweetalert2";

export default function ItemListaVen(props) {
  function confirmaExclusao(id, cliente) {
    // if (confirm(`Confirma Exclusão do Filme "${titulo}"?`)) {
    //   props.exclui(id)
    // }
    Swal.fire({
      title: `Confirma Exclusão da venda do "${cliente}"?`,
      text: "Esta operação não poderá ser desfeita",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim. Excluir!",
    }).then((result) => {
      if (result.isConfirmed) {
        props.exclui(id);
        Swal.fire("Excluído!", "venda excluida com sucesso", "success");
      }
    });
  }

  return (
    <tr>
      
      <td className={props.venda.destaque ? "fw-bold" : ""}>
        {props.venda.produto}
      </td>
      
      <td className={props.venda.destaque ? "fw-bold" : ""}>
        {props.venda.marca}
      </td>
      
      
      <td className={props.venda.destaque ? "fw-bold" : ""}>
        {props.venda.cliente}
      </td>
      <td className={props.venda.destaque ? "fw-bold" : ""}>
        {props.venda.quantidade}
      </td>
      
        <td>
          <i
            className="bi bi-trash-fill text-danger"
            style={{ fontSize: 24, cursor: "pointer" }}
            onClick={() =>
              confirmaExclusao(props.venda.id, props.venda.cliente)
            }
            title="Excluir"
          ></i>
          <i
            className="bi bi-pencil-square text-warning ms-2"
            style={{ fontSize: 24, cursor: "pointer" }}
            onClick={props.altera}
            title="Alterar"
          ></i>
          
        </td>
    </tr>
  );
}
