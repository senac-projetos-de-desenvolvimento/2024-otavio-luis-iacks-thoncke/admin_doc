import Link from "next/link";

export default function Navbar({ showNavbar }) {
  return (
    <>
      {showNavbar && (
        <nav className="navbar navbar-expand-lg bg-white shadow-sm">
          <div className="container">
            <Link className="navbar-brand" href="/Home">
              <img
                src="/logo.png"
                alt="Logo"
                width="300"
                height="80"
                className="d-inline-block align-text-top"
              />
              <h2 className="float-end mt-2 ms-2 text-dark"></h2>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link className="nav-link text-dark fs-5 mx-3" href="/listagem">
                    Produtos
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link text-dark fs-5 mx-3" href="/cadastro">
                    Cadastro
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link text-dark fs-5 mx-3" href="/ListVenda">
                    Vendas
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link text-dark fs-5 mx-3" href="/ListCliente">
                    Clientes
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link text-dark fs-5 mx-3" href="/Relatorio">
                    Clientes
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      )}
    </>
  );
}
