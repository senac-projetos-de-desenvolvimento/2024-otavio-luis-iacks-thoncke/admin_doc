import { useForm } from "react-hook-form"

export default function Pesquisa(props) {

  const { register, handleSubmit } = useForm()

  return (
    <form className="row row-cols-lg-auto g-3 align-items-center"
      onSubmit={handleSubmit(props.filtra)}
      onReset={props.mostra}>
      <div className="col-12">
        <input type="text" className="form-control"
          placeholder="Pesquisa por nome"
          {...register("pesq")}
        />
      </div>
      <div className="col-12">
        <button className="btn btn-primary" type="submit">Pesquisar</button>
      </div>
    </form>
  )
}
