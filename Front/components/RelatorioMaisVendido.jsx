'use client';

import { useEffect, useState } from "react";
import Navbar from '@/components/Titulo.jsx';

export default function RelatorioMaisVendido() {
  const [relatorio, setRelatorio] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function fetchRelatorio() {
      try {
        const response = await fetch('http://localhost:3006/gerar');
        if (!response.ok) {
          throw new Error('Erro ao carregar o relatório.');
        }
        const data = await response.json();
        setRelatorio(data);
      } catch (err) {
        setError(err.message);
        console.error('Erro:', err);
      } finally {
        setLoading(false);
      }
    }
    fetchRelatorio();
  }, []);

  return (
    <>
      <Navbar showNavbar={true} /> {/* Exibe a Navbar */}
      <div className="container" style={{ textAlign: 'center', marginTop: '20px' }}>
        <h1>Relatório do Material Mais Vendido</h1>
        {loading && <p>Carregando...</p>}
        {error && <p className="alert alert-danger">{error}</p>}
        {!loading && !error && (
          relatorio ? (
            <div>
              <p><strong>Produto:</strong> {relatorio.produto?.nome}</p>
              <p><strong>Marca:</strong> {relatorio.produto?.marca}</p>
              <p><strong>Total Vendido:</strong> {relatorio.totalVendido}</p>
            </div>
          ) : (
            <p>Nenhum produto encontrado.</p>
          )
        )}
      </div>
    </>
  );
}
