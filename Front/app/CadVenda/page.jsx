'use client'
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from '@/components/Titulo.jsx';
import { useEffect, useState } from "react";

export default function Cadastro() {
  const { register, handleSubmit, reset, setValue } = useForm({
    defaultValues: {
      duracao: "90 min",
      classif: "10",
    }
  });

  const [produtos, setProdutos] = useState([]);
  const [produtoSelecionado, setProdutoSelecionado] = useState(null);

  useEffect(() => {
    // Buscar os produtos da API
    async function getProdutos() {
      try {
        const response = await fetch("http://localhost:3006/produtos");
        const data = await response.json();
        setProdutos(data);
      } catch (error) {
        toast.error("Erro ao carregar os produtos");
      }
    }

    getProdutos();
  }, []);

  useEffect(() => {
    if (produtoSelecionado) {
      // Preencher automaticamente os campos produto e marca
      setValue("produto", produtoSelecionado.nome);
      setValue("marca", produtoSelecionado.marca);
    }
  }, [produtoSelecionado, setValue]);

  async function enviaDados(data) {
    try {
      const produto = await fetch("http://localhost:3006/vender", {
        method: "POST",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ ...data })
      });

      if (produto.status === 201) {
        toast.success("Ok! venda cadastrada com sucesso");
        reset();
      } else {
        toast.error("Erro... Não foi possível concluir o cadastro");
      }
    } catch (error) {
      toast.error("Erro na conexão... Verifique o servidor.");
    }
  }

  return (
    <>
      <Navbar showNavbar={true} /> {/* Navbar fora do container */}
      <div className="container">
        <form onSubmit={handleSubmit(enviaDados)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="produto" className="form-label">Produto</label>
              <input
                type="text"
                className="form-control"
                id="produto"
                {...register("produto")}
                required
                onClick={() => setProdutoSelecionado(produtos[0])} // Exemplo de seleção
              />
            </div>
            <div className="col-sm-4">
              <label htmlFor="marca" className="form-label">Marca</label>
              <input
                type="text"
                className="form-control"
                id="marca"
                {...register("marca")}
                required
              />
            </div>
            <div className="col-sm-2">
              <label htmlFor="cliente" className="form-label">Cliente</label>
              <input type="text" className="form-control" id="cliente" {...register("cliente")} required />
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-4">
              <label htmlFor="quantidade" className="form-label">Quantidade</label>
              <input type="number" className="form-control" id="quantidade" {...register("quantidade")} required />
            </div>
          </div>

          <input type="submit" value="Enviar" className="btn btn-primary me-3" />
          <input type="button" value="Limpar" className="btn btn-danger" onClick={() => reset()} />
        </form>

        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="dark"
        />
      </div>
    </>
  );
}