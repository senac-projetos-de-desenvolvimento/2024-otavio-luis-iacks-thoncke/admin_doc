'use client'
import { useState } from "react";
import Navbar from '@/components/Titulo.jsx';

export default function Relatorio() {
  const [relatorio, setRelatorio] = useState(null);
  const [error, setError] = useState(null);

  // Função para buscar o relatório
  async function getRelatorio() {
    try {
      const response = await fetch("http://localhost:3006/relatorio");
      if (!response.ok) {
        throw new Error('Erro ao buscar o relatório');
      }
      const dados = await response.json();
      setRelatorio(dados);
      setError(null); // Limpa erros anteriores
    } catch (error) {
      setError(error.message);
      console.error('Erro:', error);
    }
  }

  // Função para download do relatório
  function downloadRelatorio() {
    if (relatorio) {
      const blob = new Blob([JSON.stringify(relatorio, null, 2)], { type: 'application/json' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = 'relatorio_vendas.json';
      link.click();
      URL.revokeObjectURL(url); // Limpa a URL após o download
    }
  }

  return (
    <>
      <Navbar showNavbar={true} />
      <div className="container">
        <h1 className="mt-4">Relatório de Vendas</h1>

        {error && <div className="alert alert-danger mt-3">{error}</div>} {/* Exibe mensagens de erro */}

        <button className="btn btn-primary mt-3 mb-4" onClick={getRelatorio}>
          Gerar Relatório
        </button>

        {relatorio ? (
          <div>
            {/* Botão para download do relatório */}
            <button className="btn btn-secondary mb-4" onClick={downloadRelatorio}>
              Download do Relatório
            </button>

            {/* Mais Vendido */}
            <div className="card mb-3">
              <div className="card-body">
                <h4 className="card-title">Produto Mais Vendido</h4>
                <p><strong>Produto:</strong> {relatorio.maisVendido.produto}</p>
                <p><strong>Quantidade:</strong> {relatorio.maisVendido.quantidade}</p>
              </div>
            </div>

            {/* Menos Vendido */}
            <div className="card mb-3">
              <div className="card-body">
                <h4 className="card-title">Produto Menos Vendido</h4>
                <p><strong>Produto:</strong> {relatorio.menosVendido.produto}</p>
                <p><strong>Quantidade:</strong> {relatorio.menosVendido.quantidade}</p>
              </div>
            </div>

            {/* Contagem Completa */}
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Contagem Completa</h4>
                <ul>
                  {Object.entries(relatorio.contagemCompleta).map(([produto, quantidade]) => (
                    <li key={produto}>
                      <strong>{produto}:</strong> {quantidade}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        ) : (
          <p>Nenhum dado no relatório ainda. Clique no botão "Gerar Relatório".</p>
        )}
      </div>
    </>
  );
}