'use client';
import { useParams } from "next/navigation";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import Navbar from '@/components/Titulo.jsx';
import 'react-toastify/dist/ReactToastify.css';

export default function Alteracao() {
  const params = useParams();
  const { register, handleSubmit, reset } = useForm();

  useEffect(() => {
    async function getClientes() {
      try {
        // Acessando todos os clientes
        const response = await fetch("http://localhost:3006/clientes");
        const dados = await response.json();

        // Encontrando o cliente pelo ID
        const cliente = dados.find((item) => item.id === parseInt(params.id));
        
        // Se o cliente for encontrado, preencher o formulário com os dados
        if (cliente) {
          reset({
            nome: cliente.nome || "",
            endereco: cliente.endereco || "",
            cep: cliente.cep || "",
            // Inclua os outros campos que forem necessários
          });
        } else {
          toast.error("Cliente não encontrado.");
        }
      } catch (error) {
        console.error("Erro ao carregar os dados dos clientes:", error);
        toast.error("Erro ao carregar os dados.");
      }
    }

    getClientes();
  }, [params.id, reset]);

  async function alteraDados(data) {
    try {
      const cliente = await fetch(`http://localhost:3006/cliente_editar/${params.id}`, {
        method: "PUT",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify(data),
      });

      if (cliente.status === 200) {
        toast.success("Cliente alterado com sucesso");
      } else {
        toast.error("Erro... Não foi possível concluir a alteração");
      }
    } catch (error) {
      toast.error("Erro ao tentar alterar o cliente.");
    }
  }

  return (
    <div className="container">
      <Navbar showNavbar={true} />
      <form onSubmit={handleSubmit(alteraDados)}>
        <div className="row">
          <div className="col-sm-6">
            <label htmlFor="nome" className="form-label">Nome</label>
            <input type="text" className="form-control" id="nome" {...register("nome")} required />
          </div>
          <div className="col-sm-6">
            <label htmlFor="endereco" className="form-label">Endereço</label>
            <input type="text" className="form-control" id="endereco" {...register("endereco")} required />
          </div>
        </div>

        <div className="row mt-3">
          <div className="col-sm-6">
            <label htmlFor="cep" className="form-label">CEP</label>
            <input type="text" className="form-control" id="cep" {...register("cep")} required />
          </div>

          {/* Outros campos podem ser adicionados aqui conforme necessário */}
        </div>

        <input type="submit" value="Alterar" className="btn btn-success me-3" />
        <input type="button" value="Limpar" className="btn btn-danger" onClick={() => reset()} />
      </form>

      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />
    </div>
  );
}
