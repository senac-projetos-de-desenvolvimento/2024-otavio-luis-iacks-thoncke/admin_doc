'use client'
import { useEffect, useState } from "react"
import ItemLista from "@/components/clienteInfo"
import { useRouter } from "next/navigation"
import Navbar from '@/components/Titulo.jsx';

export default function Listagem() {
  const [clientes, setClientes] = useState([]);
  const [pesquisa, setPesquisa] = useState('');
  const [error, setError] = useState(null);
  
  const router = useRouter();

  // Função para buscar todos os clientes
  async function getClientes() {
    try {
      const response = await fetch("http://localhost:3006/clientes");
      if (!response.ok) {
        throw new Error('Erro ao buscar os clientes');
      }
      const dados = await response.json();
      setClientes(dados);
    } catch (error) {
      setError(error.message);
      console.error('Erro:', error);
    }
  }

  useEffect(() => {
    getClientes();
  }, []);

  async function excluiClientes(id) {
    try {
      await fetch("http://localhost:3006/deletar/" + id, { method: "DELETE" });
      const novosDados = clientes.filter(cliente => cliente.id !== id);
      setClientes(novosDados);
    } catch (error) {
      setError('Erro ao excluir cliente');
      console.error('Erro:', error);
    }
  }

  const listaClientes = clientes.map(cliente => (
    <ItemLista key={cliente.id}
      cliente={cliente}
      exclui={() => excluiClientes(cliente.id)}
      altera={() => router.push('alteraClien/' + cliente.id)}
    />
  ));

  useEffect(() => {
    if (pesquisa === '') {
      getClientes(); // Recarregar todos os clientes se a pesquisa estiver vazia
    } else {
      const novosDados = clientes.filter(cliente =>
        cliente.nome.toUpperCase().includes(pesquisa.toUpperCase())
      );
      setClientes(novosDados);
    }
  }, [pesquisa]);

  return (
    <>
      <Navbar showNavbar={true} />
      <div className="container">
        <div className="row justify-content-between align-items-center">
          <div className="col-sm-8">
            <input 
              type="text" 
              value={pesquisa} 
              onChange={e => setPesquisa(e.target.value)} 
              placeholder="Pesquisar clientes"
              className="form-control"
            />
          </div>
          <div className="col-sm-4 text-right">
            <button className="btn btn-primary btn-sm" onClick={() => router.push('/CadCliente')}>
              <i className="fas fa-plus"> +</i>
            </button>
          </div>
        </div>

        {error && <div className="alert alert-danger mt-3">{error}</div>} {/* Exibe mensagens de erro */}
        
        <div className="row mt-3">
          <div className="col-sm-12">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Endereço</th>
                  <th>Cep</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {listaClientes.length > 0 ? listaClientes : <tr><td colSpan="4">Nenhum cliente encontrado.</td></tr>}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}
