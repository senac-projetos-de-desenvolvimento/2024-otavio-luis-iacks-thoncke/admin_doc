'use client';
import { useParams } from "next/navigation";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import Navbar from '@/components/Titulo.jsx';
import 'react-toastify/dist/ReactToastify.css';

export default function Alteracao() {
  const params = useParams();
  const { register, handleSubmit, reset } = useForm();
  const [venda, setVenda] = useState(null); // Estado para armazenar o produto

  useEffect(() => {
    async function getVendas() {
      try {
        // Buscando os produtos
        const response = await fetch("http://localhost:3006/vendas");
        if (!response.ok) {
          throw new Error("Erro ao buscar as");
        }

        const dados = await response.json();

        // Buscando o produto específico com o ID
        const vendaEncontrada = dados.find((item) => item.id === parseInt(params.id));

        if (vendaEncontrada) {
          setVenda(vendaEncontrada); // Atualiza o estado com os dados do produto
          reset({
            produto: vendaEncontrada.produto || "",
            marca: vendaEncontrada.marca || "",
            cliente: vendaEncontrada.cliente|| "",
            quantidade: vendaEncontrada.quantidade || "",
          });
        } else {
          toast.error("Venda não encontrado.");
        }
      } catch (error) {
        console.error("Erro ao carregar os dados da  venda:", error);
        toast.error("Erro ao carregar os dados da venda.");
      }
    }

    // Chama a função para buscar o produto
    getVendas();
  }, [params.id, reset]);

  async function alteraDados(data) {
    try {
      const response = await fetch("http://localhost:3006/venda_editar/" + params.id, {
        method: "PUT",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ ...data })
      });a

      if (response.ok) {
        toast.success("Ok! Vendda alterada com sucesso");
      } else {
        toast.error("Erro... Não foi possível concluir a alteração");
      }
    } catch (error) {
      toast.error("Erro... Não foi possível concluir a alteração");
    }
  }

  // Se o produto não estiver carregado, exibe um "Carregando..."
  if (!venda) {
    return <div>Carregando...</div>;
  }

  return (
    <>
      <Navbar showNavbar={true} />
      <div className="container">
        <form onSubmit={handleSubmit(alteraDados)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="produto" className="form-label">Produto</label>
              <input
                type="text"
                className="form-control"
                id="produto"
                {...register("produto")}
                required
              />
            </div>
            <div className="col-sm-4">
              <label htmlFor="marca" className="form-label">Marca</label>
              <select
                className="form-control"
                id="marca"
                {...register("marca")}
                required
              >
                <option value="">Selecione uma marca</option>
                <option value="Durafloor">Durafloor</option>
                <option value="Santa Luzia">Santa Luzia</option>
                <option value="Quick Step" >Quick Step</option>
                <option value="Gradus" >Gradus</option>
              </select>
            </div>
            <div className="col-sm-2">
              <label htmlFor="cliente" className="form-label">Cliente</label>
              <input
                type="text"
                step="0.10"
                className="form-control"
                id="cliente"
                {...register("cliente")}
                required
              />
            </div>
          </div>

          <div className="row mt-3">
            <div className="col-sm-4">
              <label htmlFor="quantidade" className="form-label">Quantidade</label>
              <input
                type="number"
                className="form-control"
                id="quantidade"
                {...register("quantidade")}
                required
              />
            </div>
          </div>

          <input type="submit" value="Alterar" className="btn btn-success me-3" />
          <input type="button" value="Limpar" className="btn btn-danger" onClick={() => reset()} />
        </form>

        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="dark"
        />
      </div>
    </>
  );
}
