import 'bootstrap/dist/css/bootstrap.css'
import Titulo from '@/components/Titulo'

export const metadata = {
  title: 'estoque',
  description: 'controle dde estooque',
}

export default function RootLayout({ children }) {
  return (
    <html lang="pt-br">  
    <head>
      <link rel="shortcut icon" href="../o.png" type="image/x-icon" />  
    </head>    
      <body>
          <Titulo />
          {children}
      </body>
    </html>
  )
}
