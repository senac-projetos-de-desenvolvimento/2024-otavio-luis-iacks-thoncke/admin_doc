import './styles.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from '@/components/Titulo.jsx';
// Supondo que você tenha o componente Navbar separado

export default function Home() {
  return (
    <div>
      {/* Exibe a navbar */}
      <Navbar showNavbar={true} />

      {/* Carrossel */}
      <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
        <ol className="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              className="d-block w-100"
              src="https://durafloor-prd-images-bucket.s3.amazonaws.com/2019/08/S_Ambiente_Nature_Pucon.jpg"
              alt="First slide"
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://www.surielrevestimentos.com.br/porcelanato-delta/product/santaluziamolduras-revestimentos-linha-urbanbrick-algodao-egipcio/?"
              alt="Second slide"
            />
          </div>
          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="https://www.compracampea.com.br/wp-content/uploads/2023/07/Foto-Ambiente-Piso-Laminado-Classic-Carvalho-Neblina-Cinza.jpeg"
              alt="Third slide"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
