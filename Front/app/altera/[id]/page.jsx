'use client';
import { useParams } from "next/navigation";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import Navbar from '@/components/Titulo.jsx';
import 'react-toastify/dist/ReactToastify.css';

export default function Alteracao() {
  const params = useParams();
  const { register, handleSubmit, reset } = useForm();
  const [produto, setProduto] = useState(null); // Estado para armazenar o produto

  useEffect(() => {
    async function getProdutos() {
      try {
        // Buscando os produtos
        const response = await fetch("http://localhost:3006/produtos");
        if (!response.ok) {
          throw new Error("Erro ao buscar os produtos.");
        }

        const dados = await response.json();

        // Buscando o produto específico com o ID
        const produtoEncontrado = dados.find((item) => item.id === parseInt(params.id));

        if (produtoEncontrado) {
          setProduto(produtoEncontrado); // Atualiza o estado com os dados do produto
          reset({
            nome: produtoEncontrado.nome || "",
            marca: produtoEncontrado.marca || "",
            preco: produtoEncontrado.preco || "",
            quantidade: produtoEncontrado.quantidade || "",
          });
        } else {
          toast.error("Produto não encontrado.");
        }
      } catch (error) {
        console.error("Erro ao carregar os dados do produto:", error);
        toast.error("Erro ao carregar os dados do produto.");
      }
    }

    // Chama a função para buscar o produto
    getProdutos();
  }, [params.id, reset]);

  async function alteraDados(data) {
    try {
      const response = await fetch("http://localhost:3006/produto_editar/" + params.id, {
        method: "PUT",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ ...data })
      });

      if (response.ok) {
        toast.success("Ok! Produto alterado com sucesso");
      } else {
        toast.error("Erro... Não foi possível concluir a alteração");
      }
    } catch (error) {
      toast.error("Erro... Não foi possível concluir a alteração");
    }
  }

  // Se o produto não estiver carregado, exibe um "Carregando..."
  if (!produto) {
    return <div>Carregando...</div>;
  }

  return (
    <>
      <Navbar showNavbar={true} />
      <div className="container">
        <form onSubmit={handleSubmit(alteraDados)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="nome" className="form-label">Nome</label>
              <input
                type="text"
                className="form-control"
                id="nome"
                {...register("nome")}
                required
              />
            </div>
            <div className="col-sm-4">
              <label htmlFor="marca" className="form-label">Marca</label>
              <select
                className="form-control"
                id="marca"
                {...register("marca")}
                required
              >
                <option value="">Selecione uma marca</option>
                <option value="Durafloor" selected={produto.marca === "Durafloor"}>Durafloor</option>
                <option value="Santa Luzia" selected={produto.marca === "Santa Luzia"}>Santa Luzia</option>
                <option value="Quick Step" selected={produto.marca === "Quick Step"}>Quick Step</option>
                <option value="Gradus" selected={produto.marca === "Gradus"}>Gradus</option>
              </select>
            </div>
            <div className="col-sm-2">
              <label htmlFor="preco" className="form-label">Preço R$</label>
              <input
                type="number"
                step="0.10"
                className="form-control"
                id="preco"
                {...register("preco")}
                required
              />
            </div>
          </div>

          <div className="row mt-3">
            <div className="col-sm-4">
              <label htmlFor="quantidade" className="form-label">Quantidade</label>
              <input
                type="number"
                className="form-control"
                id="quantidade"
                {...register("quantidade")}
                required
              />
            </div>
          </div>

          <input type="submit" value="Alterar" className="btn btn-success me-3" />
          <input type="button" value="Limpar" className="btn btn-danger" onClick={() => reset()} />
        </form>

        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="dark"
        />
      </div>
    </>
  );
}
