"use client"
import { useEffect, useState } from "react";
import ItemListaVen from "@/components/ItemListaVen";
import { useRouter } from "next/navigation";
import Navbar from '@/components/Titulo.jsx';

export default function Listagem() {
  const [vendas, setVendas] = useState([]);
  const [pesquisa, setPesquisa] = useState('');
  const [error, setError] = useState(null);
  const router = useRouter();

  // Função para buscar todas as vendas
  async function getVendas() {
    try {
      const response = await fetch("http://localhost:3006/vendas");
      if (!response.ok) {
        throw new Error('Erro ao buscar vendas');
      }
      const dados = await response.json();
      setVendas(dados);
    } catch (error) {
      setError(error.message);
      console.error('Erro:', error);
    }
  }

  useEffect(() => {
    getVendas();
  }, []);

  // Função para excluir uma venda
  async function excluiVendas(id) {
    try {
      await fetch("http://localhost:3006/venda_delete/" + id, {
        method: "DELETE"
      });
      const novosDados = vendas.filter(venda => venda.id !== id);
      setVendas(novosDados);
    } catch (error) {
      setError('Erro ao excluir venda');
      console.error('Erro:', error);
    }
  }

  // Filtragem das vendas com base na pesquisa
  useEffect(() => {
    if (pesquisa === '') {
      getVendas(); // Recarregar todas as vendas se a pesquisa estiver vazia
    } else {
      const novosDados = vendas.filter(venda =>
        venda.produto.toUpperCase().includes(pesquisa.toUpperCase()) ||
        venda.marca.toUpperCase().includes(pesquisa.toUpperCase()) ||
        venda.cliente.toUpperCase().includes(pesquisa.toUpperCase())
      );
      setVendas(novosDados);
    }
  }, [pesquisa]);

  const listaVendas = vendas.map(venda => (
    <ItemListaVen key={venda.id}
      venda={venda}
      exclui={() => excluiVendas(venda.id)}
      altera={() => router.push('alteraVend/' + venda.id)}
    />
  ));

  return (
    <>
      <Navbar showNavbar={true} /> {/* Navbar fora do container */}
      <div className="container">
        <div className="row justify-content-between align-items-center">
          <div className="col-sm-8">
            <input 
              type="text" 
              value={pesquisa} 
              onChange={e => setPesquisa(e.target.value)} 
              placeholder="Pesquisar vendas"
              className="form-control"
            />
          </div>
          <div className="col-sm-4 text-right">
            <button className="btn btn-primary btn-sm" onClick={() => router.push('/CadVenda')}>
              <i className="fas fa-plus"> +</i>
            </button>
          </div>
        </div>

        {error && <div className="alert alert-danger mt-3">{error}</div>} {/* Exibe mensagens de erro */}
        
        <div className="row mt-3">
          <div className="col-sm-12">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Marca</th>
                  <th>Cliente</th>
                  <th>Quantidade</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {listaVendas.length > 0 ? listaVendas : <tr><td colSpan="5">Nenhuma venda encontrada.</td></tr>}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}