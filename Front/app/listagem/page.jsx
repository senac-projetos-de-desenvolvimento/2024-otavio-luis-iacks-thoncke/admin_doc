'use client'
import { useEffect, useState } from "react"
import ItemLista from "@/components/ItemLista"
import { useRouter } from "next/navigation"
import Navbar from '@/components/Titulo.jsx';

export default function Listagem() {
  
  const [produtos, setProdutos] = useState([])
  const [pesquisa, setPesquisa] = useState('');  
  const [opcaoOrdenacao, setOpcaoOrdenacao] = useState('')

  const router = useRouter()

  useEffect(() => {
    async function getProdutos() {
      const response = await fetch("http://localhost:3006/produtos")
      const dados = await response.json()
      setProdutos(dados)
    }
    getProdutos()
  }, [])

  async function excluiProdutos(id) {
    const response = await fetch("http://localhost:3006/produto_delete/" + id, {
      method: "DELETE"
    })
    const novosDados = produtos.filter(produtos => produtos.id != id)
    setProdutos(novosDados)
  }

  const listaProdutos = produtos.map(produto => (
    <ItemLista key={produto.id}
    produto={produto}
      exclui={() => excluiProdutos(produto.id)}
      altera={() => router.push('altera/' + produto.id)}
    />
  ))

  function BuscaProdutos(data, setProdutos) {
    async function getProdutos() {
      try {
        const response = await fetch("http://localhost:3006/produtos");
        if (!response.ok) {
          throw new Error('Erro ao buscar os produtos');
        }
        const dados = await response.json();

        if (!data || !data.pesq) {
          setProdutos(dados);
        } else {
          const pesquisa = data.pesq.toUpperCase();
          const novosDados = dados.filter(produto =>
            produto.nome.toUpperCase().includes(pesquisa)
          );
          setProdutos(novosDados);
        }
      } catch (error) {
        console.error('Erro:', error);
      }
    }
    getProdutos();
  }

  useEffect(() => {
    BuscaProdutos({ pesq: pesquisa }, setProdutos);
  }, [pesquisa])

  return (
    <>
      <Navbar showNavbar={true} /> {/* Navbar fora do container */}
      <div className="container">
        <div className="row">
          <h2 className="col-sm-6 mt-2">Lista de Produtos</h2>
          <div>
            <input 
              type="text" 
              value={pesquisa} 
              onChange={e => setPesquisa(e.target.value)} 
              placeholder="Pesquisar produtos"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 mt-3">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Marca</th>
                  <th>Preço</th>
                  <th>Quantidade</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {listaProdutos}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}
