'use client';

import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default function Cadastro() {
  const { register, handleSubmit, reset } = useForm();

  async function enviaDados(data) {
    try {
      const response = await fetch("http://localhost:3006/registrar", {
        method: "POST",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify(data)
      });

      if (response.status === 201) {
        toast.success("Ok! Registro concluído com sucesso!");
        reset();
      } else {
        toast.error("Erro: Não foi possível concluir o cadastro.");
      }
    } catch (error) {
      toast.error("Erro de conexão. Verifique o servidor.");
    }
  }

  return (
    <>
      
      <div className="container">
        <h1 className="my-4">Cadastro de Cliente</h1>
        <form onSubmit={handleSubmit(enviaDados)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="nome" className="form-label">Nome</label>
              <input type="text" className="form-control" id="nome" {...register("nome")} required />
            </div>
            <div className="col-sm-6">
              <label htmlFor="senha" className="form-label">Senha</label>
              <input type="password" className="form-control" id="senha" {...register("senha")} required />
            </div>
          </div>
          <div className="row mt-3">
            

          </div>
          <div className="mt-4">
            <input type="submit" value="Enviar" className="btn btn-primary me-3" />
            
          </div>
        </form>

        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="dark"
        />
      </div>
    </>
  );
}
