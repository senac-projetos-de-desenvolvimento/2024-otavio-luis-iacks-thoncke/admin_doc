'use client'
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from '@/components/Titulo.jsx';

export default function Cadastro() {
  const { register, handleSubmit, reset } = useForm({
    defaultValues: {
      duracao: "90 min",
      classif: "10"
    }
  });

  async function enviaDados(data) {
    try {
      const produto = await fetch("http://localhost:3006/cadastrar_cliente", {
        method: "POST",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ ...data })
      });

      if (produto.status === 201) {
        toast.success("Ok! venda cadastrada com sucesso");
        reset();
      } else {
        toast.error("Erro... Não foi possível concluir o cadastro");
      }
    } catch (error) {
      toast.error("Erro na conexão... Verifique o servidor.");
    }
  }

  return (
    <>
      <Navbar showNavbar={true} /> {/* Navbar fora do container */}
      <div className="container">
        <form onSubmit={handleSubmit(enviaDados)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="nome" className="form-label">Nome</label>
              <input type="text" className="form-control" id="nome" {...register("nome")} required />
            </div>
            
            <div className="col-sm-2">
              <label htmlFor="endereco" className="form-label">Endereço</label>
              <input type="text" className="form-control" id="endereco" {...register("endereco")} required />
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-4">
              <label htmlFor="cep" className="form-label">Cep</label>
              <input type="number" className="form-control" id="cep" {...register("cep")} required />
            </div>
          </div>

          <input type="submit" value="Enviar" className="btn btn-primary me-3" />
          <input type="button" value="Limpar" className="btn btn-danger" onClick={() => reset()} />
        </form>

        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="dark"
        />
      </div>
    </>
  );
}
