'use client'
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Navbar from '@/components/Titulo.jsx';

export default function Cadastro() {
  const { register, handleSubmit, reset } = useForm({
    defaultValues: {
      duracao: "90 min",
      classif: "10"
    }
  });

  async function enviaDados(data) {
    try {
      const produto = await fetch("http://localhost:3006/cadastrar", {
        method: "POST",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ ...data })
      });

      if (produto.status == 201) {
        toast.success("Ok! Produto cadastrado com sucesso");
        reset();
      } else {
        toast.error("Erro... Não foi possível concluir o cadastro");
      }
    } catch (error) {
      toast.error("Erro na conexão... Verifique o servidor.");
    }
  }

  return (
    <>
      <Navbar showNavbar={true} /> {/* Navbar fora do container */}
      <div className="container">
        <form onSubmit={handleSubmit(enviaDados)}>
          <div className="row">
            <div className="col-sm-6">
              <label htmlFor="nome" className="form-label">Nome</label>
              <input type="text" className="form-control" id="nome" {...register("nome")} required />
            </div>
            <div className="col-sm-4">
              <label htmlFor="marca" className="form-label">Marca</label>
              <select className="form-control" id="marca" {...register("marca")} required>
                <option value="">Selecione uma marca</option>
                <option value="Durafloor">Durafloor</option>
                <option value="Santa Luzia">Santa Luzia</option>
                <option value="Quick Step">Quick Step</option>
                <option value="Gradus">Gradus</option>
              </select>
            </div>
            <div className="col-sm-2">
              <label htmlFor="preco" className="form-label">Preço R$</label>
              <input type="number" step="0.10" className="form-control" id="preco" {...register("preco")} required />
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-sm-4">
              <label htmlFor="quantidade" className="form-label">Quantidade</label>
              <input type="number" className="form-control" id="quantidade" {...register("quantidade")} required />
            </div>
          </div>

          <input type="submit" value="Enviar" className="btn btn-primary me-3" />
          <input type="button" value="Limpar" className="btn btn-danger" onClick={() => reset()} />
        </form>

        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="dark"
        />
      </div>
    </>
  );
}
